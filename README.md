## 🚀 Prueba práctica de Java perfil Junior 🚀

### Límite de tiempo
* 1 hora (60 minutos)

### Pre-requisitos
* Se necesita instalar Java Development Kit versión 8 o superior para compilar y correr la prueba
  (https://www.oracle.com/co/java/technologies/javase-downloads.html)
* IDE de su preferencia (IntelliJ, Eclipse, NetBeans), en el que se sienta más cómodo
* Configurar el JDK versión 8 o superior en el IDE elegido

### Conocimientos requeridos
* Entender qué es un archivo CSV
* Estructuras de datos
* Java 
  * Manejo y procesamiento de colecciones
  * Lectura de archivos de texto

### Qué se evaluará
#### Todos
* Cumplimiento del programa de la funcionalidad solicitada
* Nombres adecuados de variables
* Uso de abstracciones y estructuras de datos
#### Seniors
* Mantenibilidad del código
* Manejo adecuado de excepciones
* Uso adecuado de recursos
* Eficiencia del programa

### Descripción funcional de la prueba
En la carpeta `src/main/resources` del repositorio se encuentra un archivo llamado `empleados.csv`, que contiene
registros de los empleados de una empresa con la siguiente información:

* nombre: Nombre del empleado
* departamento: Departamento de la empresa en la cual el empleado labora
* salario: Salario del empleado
* antiguedad: Antigüedad en años del empleado en la empresa

A continuación se muestra un ejemplo visual de los datos del archivo:

| nombre | departamento | salario | antigüedad|
|  ---   |     ---      |  ---    |   ---     | 
| empleado1 | Marketing | 2,100,000 | 2  |
| empleado2 | Comercial | 1,900,000 | 5  |
| empleado3 | Comercial | 1,900,000 | 7  |
| empleado4 | Logística | 1,700,000 | 1  |
| empleado5 | Logística | 2,500,000 | 6  |
| empleado6 | Logística | 2,500,000 | 7  |
| empleado7 | Financiero| 2,800,000 | 10 |
| empleado8 | Financiero| 2,100,000 | 6  |

El gerente general requiere que usted implemente una aplicación en Java que a partir de la información en el archivo
produzca un informe que permita conocer cuanto suman los salarios de los empleados de cada departamento de la empresa, 
teniendo en cuenta solo los empleados con una antigüedad mayor o igual a doce años. Salida de ejemplo del programa:
```
Comercial: 3800000
Logística: 5000000
Financiero: 4900000
```
### Requerimientos técnicos

* La lógica que produce el informe debe implementarse en el método llamado `totalSalarioPorDepartamento` de la clase 
  `com.example.demo.DemoApplication`. Este método retorna un tipo `List<TotalSalariosXDepartamento>` con el listado de
  los diferentes departamentos, cada uno con el valor total de salarios de sus empleados. Se recomienda ver la clase
  `TotalSalariosXDepartamento`
* NO se debe modificar la firma del método `totalSalarioPorDepartamento` en la clase `com.example.demo.DemoApplication`
  , es decir, no pueden cambiar ni su nombre, parámetros (nombres, tipo, orden), ni su tipo de retorno ni agregar
  declaración de lanzamiento de excepciones (Ej. `throws Exception`)
* NO debe modificar los registros del archivo `src/main/resources/empleados.csv` y además tenga en cuenta que los valores
  descritos antes en este README.md son sólo ilustrativos, por lo tanto se espera que con la lectura real del archivo
  `empleados.csv` se retornen sumatorias diferentes