package com.example.demo;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class DemoApplicationTests {

	//Esta prueba unitaria aplica para evaluar los resultados cuya antigüedad de los empleados sea mayor o igual a 12 años

	@Test
	@DisplayName("Totales de salarios por departamento sólo considerando empleados con antiguedad >= 12 años")
	void totalesSalariosXDeptos_cuando_empleadosAntiguedadMayorIgual12Agnos() {
		DemoApplication application = new DemoApplication();
		List<TotalSalariosXDepartamento> response = application.totalSalarioPorDepartamento();

		TotalSalariosXDepartamento control = new TotalSalariosXDepartamento("Control",9772295);
		TotalSalariosXDepartamento finanzas = new TotalSalariosXDepartamento("Finanzas",5926870);
		TotalSalariosXDepartamento compras = new TotalSalariosXDepartamento("Compras",7579040);
		TotalSalariosXDepartamento desarrollo = new TotalSalariosXDepartamento("Desarrollo",17376015);
		TotalSalariosXDepartamento gerencia = new TotalSalariosXDepartamento("Gerencia",6471581);
		TotalSalariosXDepartamento marketing = new TotalSalariosXDepartamento("Marketing",25339358);
		assertThat(response)
				.isNotEmpty()
				.hasSize(6)
				.contains(control)
				.contains(finanzas)
				.contains(compras)
				.contains(desarrollo)
				.contains(gerencia)
				.contains(marketing);
	}

}
