package com.example.demo;

import java.util.Objects;

public class TotalSalariosXDepartamento {
    private String departamento;
    private long totalSalarios;

    @Override
    public int hashCode() {
        return Objects.hash(departamento, totalSalarios);
    }

    public TotalSalariosXDepartamento(String departamento, long totalSalarios) {
        this.departamento = departamento;
        this.totalSalarios = totalSalarios;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public long getTotalSalarios() {
        return totalSalarios;
    }

    public void setTotalSalarios(long totalSalarios) {
        this.totalSalarios = totalSalarios;
    }

    @Override
    public String toString() {
        return "TotalSalariosXDepartamento{" +
                "departamento='" + departamento + '\'' +
                ", totalSalarios=" + totalSalarios +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TotalSalariosXDepartamento that = (TotalSalariosXDepartamento) o;
        return totalSalarios == that.totalSalarios && Objects.equals(departamento, that.departamento);
    }
}
